using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Reflection;

[CustomEditor(typeof(Interactable))]
public class InteractableEditor : Editor
{
	#region Constants
	
	
	static readonly float PARAMETER_INDENTATION = 28;
	static readonly float ANIM_CURVE_HEIGHT = 75;
	
	static readonly Color INVALID_SCRIPT_COLOR = new Color(0.8f, 0.6f, 0.6f);
	static readonly Color VALID_SCRIPT_COLOR = new Color(0.6f, 0.8f, 0.6f);
	
	
	#endregion
	
	
	#region Fields
	
	
	Interactable instance;
	Interactable prefab;
	
	
	#endregion
	
	
	#region Editor event handlers
	
	
	public void OnEnable()
	{		
		instance = (Interactable)target;
		
		UnityEngine.Object o = PrefabUtility.GetPrefabParent(instance);
		
		if (o is Interactable)
			prefab = (Interactable)o;
		else
			prefab = null;
	}
	
	public override void OnInspectorGUI()
	{
		if (instance == null)
			return;
		
		DrawDefaultInspector();

		EditorGUILayout.Separator();
		DrawSingleInteractionSection(instance.SingleInteractions);
		EditorGUILayout.Separator();
		DrawDualInteractionSection(instance.DualInteractions);
		EditorGUILayout.Separator();
		
		if (GUI.changed || Application.isPlaying)
			EditorUtility.SetDirty(target);
	}
	
	
	#endregion
	
	
	#region Drawing interactions
	
	
	/// <summary>
	/// Draws single interaction section in inspector with foldout.
	/// </summary>
	void DrawSingleInteractionSection(SingleInteraction[] interactions)
	{
		// section title / foldout
		instance.foldoutSingleSection = EditorGUILayout.Foldout(
			instance.foldoutSingleSection, "Single iScripts");
		
		if (instance.foldoutSingleSection)
			DrawSingleInteractionSet();
	}
	
	/// <summary>
	/// Draws dual interaction section in inspector with foldout.
	/// </summary>
	void DrawDualInteractionSection(DualInteraction[] interactions)
	{
		// section title / foldout
		instance.foldoutDualSection = EditorGUILayout.Foldout(
			instance.foldoutDualSection, "Dual iScripts");
		
		if (instance.foldoutDualSection)
			DrawDualInteractionSet();
	}
	
	/// <summary>
	/// Draws an enum mask field and editable options for a set of interactions.
	/// </summary>
	/// <param name='interactions'>
	/// Interactions.
	/// </param>
	void DrawSingleInteractionSet()
	{
		GUIStyle labelStyle = EditorStyles.label;
		
		if ((prefab != null) && (instance.SingleVerbs != prefab.SingleVerbs))
			labelStyle = EditorStyles.boldLabel;
		
		GUILayout.BeginHorizontal();
		
		EditorGUILayout.PrefixLabel("Select Verb(s)", "Button", labelStyle);
		instance.SingleVerbs = (SingleInteraction.VerbType)
			EditorGUILayout.EnumMaskField(instance.SingleVerbs);
		
		GUILayout.EndHorizontal();

		SingleInteraction[] instanceInteractions = instance.SingleInteractions;
		
		for (int i = 0; i < instanceInteractions.Length; i++)
			DrawInstanceInteraction(instanceInteractions[i]);
	}
	
	/// <summary>
	/// Draws an enum mask field and editable options for a set of interactions.
	/// </summary>
	/// <param name='interactions'>
	/// Interactions.
	/// </param>
	void DrawDualInteractionSet()
	{
		GUIStyle labelStyle = EditorStyles.label;
		
		if ((prefab != null) && (instance.DualVerbs != prefab.DualVerbs))
			labelStyle = EditorStyles.boldLabel;
		
		GUILayout.BeginHorizontal();
		
		EditorGUILayout.PrefixLabel("Select Verb(s)", "Button", labelStyle);
		instance.DualVerbs = (DualInteraction.VerbType)
			EditorGUILayout.EnumMaskField(instance.DualVerbs);
		
		GUILayout.EndHorizontal();
		
		DualInteraction[] instanceInteractions = instance.DualInteractions;
		
		for (int i = 0; i < instanceInteractions.Length; i++)
			DrawInstanceInteraction(instanceInteractions[i]);
	}
	
	/// <summary>
	/// Draws an interaction on instance object in inspector with field for specifying
	/// interaction script and all script parameters if valid script name is entered.
	/// </summary>
	/// <remarks>Draws interaction parameters in order of inheritance.</remarks>
	void DrawInstanceInteraction(Interaction instanceInteraction)
	{	
		string iName = StringUtility.CamelToHuman(instanceInteraction.VerbAsString);
		
		EditorGUILayout.Separator();
		
		EditorGUILayout.BeginHorizontal();
		
		EditorGUILayout.PrefixLabel("'" + iName + "' iScript");
		
		Color defaultColor = GUI.color;
		
		if (instanceInteraction.Script == null)
			GUI.color = INVALID_SCRIPT_COLOR;
		else
			GUI.color = VALID_SCRIPT_COLOR;
		
		// script name entry
		instanceInteraction.ScriptName =
			EditorGUILayout.TextField(instanceInteraction.ScriptName, GUILayout.MinWidth(40));

		GUI.color = defaultColor;
		
		GUILayout.Label("In scene", GUILayout.Width(54));		
		instanceInteraction.AvailableInScene = 
			EditorGUILayout.Toggle(instanceInteraction.AvailableInScene, GUILayout.Width(16));
		
		GUILayout.Label("In inventory", GUILayout.Width(75));
		instanceInteraction.AvailableInInventory = 
			EditorGUILayout.Toggle(instanceInteraction.AvailableInInventory, GUILayout.Width(16));
		
		EditorGUILayout.EndHorizontal();
		
		// notify to fix script name
		if ((instanceInteraction.Script == null) || 
			!instanceInteraction.Script.GetType().IsSubclassOf(typeof(InteractionScript)))
		{
			EditorGUILayout.HelpBox("Enter name of a '" + instanceInteraction.ScriptConstraint +
				"' above", MessageType.Info, false);
		}
		/*
		// draw all script parameters
		else
		{
			Type scriptType = instanceInteraction.Script.GetType();
			
			List<FieldInfo> staticFields = new List<FieldInfo>();
			List<FieldInfo> instanceFields = new List<FieldInfo>();

			// RISKY! Need to make v.sure script is subclass of 'InteractionScript'
			while (scriptType != typeof(InteractionScript).BaseType)
			{
				staticFields.InsertRange(0, scriptType.GetFields(
					BindingFlags.Public|BindingFlags.DeclaredOnly|BindingFlags.Static));
				instanceFields.InsertRange(0, scriptType.GetFields(
					BindingFlags.Public|BindingFlags.DeclaredOnly|BindingFlags.Instance));
				scriptType = scriptType.BaseType;
			}
			
			// shared / static parameters
			instanceInteraction.foldoutScriptStaticParameters = DrawInteractionParameterSet(
				staticFields, instanceInteraction.Script, "Shared Parameters", 
				instanceInteraction.foldoutScriptStaticParameters, false);
			
			// instance parameters
			instanceInteraction.foldoutScriptInstanceParameters = DrawInteractionParameterSet(
				instanceFields, instanceInteraction.Script, "Instance Parameters",
				instanceInteraction.foldoutScriptInstanceParameters, true);
		}
		*/
	}
	
	/// <summary>
	/// Draws an interaction on prefab /asset in inspector.
	/// </summary>
	/// <remarks>Draws interaction parameters in order of inheritance.</remarks>
	void DrawPrefabInteraction(Interaction interaction)
	{
		string iName = StringUtility.CamelToHuman(interaction.VerbAsString);
		
		EditorGUILayout.Separator();
		
		EditorGUILayout.BeginHorizontal();
		
		EditorGUILayout.PrefixLabel(iName + " iScript");

		GUI.enabled = false;
		
		// script name entry
		interaction.ScriptName = EditorGUILayout.TextField(
			interaction.ScriptName, GUILayout.MinWidth(40));

		GUI.enabled = true;
		
		// availability toggles
		GUILayout.Label("In scene", GUILayout.Width(56));		
		interaction.AvailableInScene = EditorGUILayout.Toggle(
			interaction.AvailableInScene, GUILayout.Width(16));
		GUILayout.Label("In inventory", GUILayout.Width(76));
		interaction.AvailableInInventory = EditorGUILayout.Toggle(
			interaction.AvailableInInventory, GUILayout.Width(16));
		
		EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.HelpBox(
			"Go to instance(s) in scene to set iScripts and parameters",
			MessageType.Info, false);
	}
	
	/// <summary>
	/// Draws an interaction parameter set, inherited before newly declared parameters.
	/// </summary>
	/// <returns>Whether set should be folded out.</returns>
	bool DrawInteractionParameterSet(List<FieldInfo> fields,
		InteractionScript script, string name, bool foldout, bool enabled)
	{
		if ((fields == null) || (fields.Count == 0))
			return foldout;
		
		EditorGUILayout.BeginHorizontal();
		GUILayout.Space(PARAMETER_INDENTATION);
		foldout = EditorGUILayout.Foldout(foldout, name);
		EditorGUILayout.EndHorizontal();
		
	    if (foldout)
		{
			GUI.enabled = enabled;
			
			foreach (FieldInfo f in fields)
			{
				EditorGUILayout.BeginHorizontal();
				GUILayout.Space(PARAMETER_INDENTATION*2);
				DrawInteractionParameter(f, script);
				EditorGUILayout.EndHorizontal();
			}
			
			GUI.enabled = true;
		}
		
		return foldout;
	}
	
	/// <summary>
	/// Draws an interaction parameter in inspector with closest matching editable field.
	/// </summary>
	/// <returns>Whether parameter could be added (i.e., has a supported type)</returns>
	bool DrawInteractionParameter(FieldInfo field, InteractionScript script)
	{
		object obj = field.GetValue(script);
		string paramName = StringUtility.CamelToHuman(field.Name);
		
		if (field.FieldType == typeof(string))
		{
			string val = (string)obj;
			if (val == default(string))
				val = "";
			
			val = EditorGUILayout.TextField(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(int))
		{
			int val = (int)obj;
			
			val = EditorGUILayout.IntField(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(float))
		{
			float val = (float)obj;
			
			val = EditorGUILayout.FloatField(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(bool))
		{
			bool val = (bool)obj;
			
			val = EditorGUILayout.Toggle(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(Bounds))
		{
			Bounds val = (Bounds)obj;
			
			val = EditorGUILayout.BoundsField(paramName, val);
			obj = val;
		}
		else if ((field.FieldType == typeof(Color)) || 
			(field.FieldType == typeof(Color32)))
		{
			Color val = (Color)obj;
			if (val == default(Color))
				val = new Color(1, 1, 1);
			
			val = EditorGUILayout.ColorField(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(AnimationCurve))
		{
			AnimationCurve val = (AnimationCurve)obj;
			
			val = EditorGUILayout.CurveField(paramName, val, 
				GUILayout.MinHeight(ANIM_CURVE_HEIGHT));
			obj = val;
		}
		else if (field.FieldType.IsEnum)
		{
			Enum val = (Enum)obj;
			
			val = EditorGUILayout.EnumPopup(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(LayerMask))
		{
			LayerMask val = (LayerMask)obj;
			
			val = EditorGUILayout.LayerField(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(Rect))
		{
			Rect val = (Rect)obj;
			
			val = EditorGUILayout.RectField(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(Vector2))
		{
			Vector2 val = (Vector2)obj;
			
			val = EditorGUILayout.Vector2Field(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(Vector3))
		{
			Vector3 val = (Vector3)obj;
			
			val = EditorGUILayout.Vector3Field(paramName, val);
			obj = val;
		}
		else if (field.FieldType == typeof(Vector4))
		{
			Vector4 val = (Vector4)obj;
			
			val = EditorGUILayout.Vector4Field(paramName, val);
			obj = val;
		}
		else if (field.FieldType.IsSubclassOf(typeof(UnityEngine.Object))
			|| field.FieldType.IsSerializable)
		{
			try
			{
				UnityEngine.Object val = (UnityEngine.Object)obj;
				
				val = EditorGUILayout.ObjectField(paramName, val, field.FieldType, 
					!EditorUtility.IsPersistent(target));
				obj = val;
			}
			catch
			{
				GUILayout.Label(paramName + " (" + field.FieldType + 
					") is unsupported in the inspector, make sure it is serializable");
				return false;
			}
		}
		
		try
		{
			field.SetValue(script, obj);
			return true;
		}
		catch
		{
			GUILayout.Label(paramName + " (" + field.FieldType + 
				") is unsupported in the inspector, make sure it is serializable");
			return false;
		}
	}
	
	
	#endregion
}
